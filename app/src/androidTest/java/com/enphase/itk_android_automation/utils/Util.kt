package com.enphase.itk_android_automation.utils

import okhttp3.*
import org.json.JSONObject
import okhttp3.RequestBody
import okhttp3.OkHttpClient


fun getRandomString(length: Int) : String {
    //val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
    val allowedChars = ('a'..'z')

    return (1..length)
        .map { allowedChars.random() }
        .joinToString("")
}

fun getRandomNumber(length: Int) : String {
    val allowedChars = ('0'..'9')

    return (1..length)
        .map { allowedChars.random() }
        .joinToString("")
}

fun createAuthToken(): String? {

    val client: OkHttpClient = OkHttpClient().newBuilder()
        .build()
    val mediaType: MediaType? = MediaType.parse("text/plain")
    val body: RequestBody = RequestBody.create(mediaType, "")
    val request: Request = Request.Builder()
        .url("https://api-qa2.enphaseenergy.com/oauth/token?grant_type=password&password=Enphase@3159&username=mwasiq@enphaseenergy.com")
        .method("POST", body)
        .addHeader("Authorization", "Basic aW5zdGFsbGVyLXRvb2xraXQ6cjBQaTEwemN4cGlKSVRaQ0V2bUwyR3h6MzZrS2RWcDBvUjZZT2pYdQ==")
        .build()

    val response: Response = client.newCall(request).execute()

    val yt = response.body()?.string()

    return if (yt == null) null else {
        val jObject = JSONObject(yt)
        jObject["access_token"].toString()
    }
}

fun createSite(site : String, token: String) : String? {

    val r1 = getRandomString(4)
    val r2 = getRandomString(3)
    val r3 = getRandomString(3)
    val email = "$r1.$r2@$r3.com"
    val fname = getRandomString(4)
    val lname = getRandomString(4)
    val pn = "5414${getRandomNumber(6)}"

    val client = OkHttpClient().newBuilder()
        .build()
    val mediaType = MediaType.parse("application/x-www-form-urlencoded")
    val body = RequestBody.create(
        mediaType,
        "system={\"address\":{\"city\":\"New York\",\"country\":\"US\",\"state\":\"NY\",\"street1\":\"Astor Place\",\"street2\":\"\",\"postal_code\":\"10003\",\"latitude\":0,\"longitude\":0},\"batteries\":[],\"battery_arrays\":[],\"envoys\":[],\"expected_acb_count\":0,\"expected_envoy_count\":0,\"expected_meter_count\":0,\"expected_pcu_count\":0,\"expected_nsr_count\":0,\"internet_connection\":\"none\",\"is_lease\":false,\"meters\":[],\"inverters\":[],\"arrays\":[],\"system_name\":\"$site\",\"owner\":{\"email\":\"$email\",\"first_name\":\"$fname\",\"last_name\":\"$lname\",\"phone\":\"$pn\"},\"nsrs\":[],\"reference\":\"Tdbj\",\"stage\":1,\"system_type\":\"other\",\"check_existing_owner\":true,\"requested_profile\":\"agf:5da3aa013da64a3fee2d7084\",\"requested_profile_name\":\"CA Rule21 201902 VV VW FW (1.2.8)\",\"system_id\":-1,\"permit_details\":{\"solar\":{\"contract_signed_date\":0,\"hardware_installation_date\":0,\"permit_ahj\":\"\",\"permit_complete_date\":0,\"permit_submission_date\":0},\"storage\":{\"contract_signed_date\":0,\"hardware_installation_date\":0,\"permit_ahj\":\"\",\"permit_complete_date\":0,\"permit_submission_date\":0}}}"
    )
    val request: Request = Request.Builder()
        .url("https://api-qa2.enphaseenergy.com/api/v4/activations")
        .method("POST", body)
        .addHeader("Content-Type", "application/x-www-form-urlencoded")
        .addHeader(
            "Authorization",
            "Bearer $token"
        )
        .build()

    val response: Response = client.newCall(request).execute()

    val yt = response.body()?.string()

    return if (yt == null) null else {
        val jObject = JSONObject(yt)
        jObject["system_id"].toString()
    }
}

fun addEnvoy(envoy : String, siteID : String, token: String) {

    val client = OkHttpClient().newBuilder()
        .build()
    val mediaType = MediaType.parse("application/json")
    val body = RequestBody.create(
        mediaType,
        "[{\"grid_profile_id\":\"agf:5da3aa013da64a3fee2d7084\",\"part_num\":\"800-00555-r01\",\"phase_num\":0,\"serial_num\":\"$envoy\"}]"
    )
    val request: Request = Request.Builder()
        .url("https://api-qa2.enphaseenergy.com/api/v3/systems/$siteID/envoys")
        .method("PUT", body)
        .addHeader("key", "33d4cec7b72267778b67d0bacded2753")
        .addHeader(
            "Authorization",
            "Bearer $token"
        )
        .addHeader("Content-Type", "application/json")
        .build()
    val response = client.newCall(request).execute()

}

fun addPCU(pcu : String, siteID : String, token: String) {

    val client = OkHttpClient().newBuilder()
        .build()
    val mediaType = MediaType.parse("application/json")
    val body = RequestBody.create(
        mediaType,
        "[{\"part_num\":\"800-00107-r01\",\"phase_num\":0,\"serial_num\":\"$pcu\"}]"
    )
    val request: Request = Request.Builder()
        .url("https://api-qa2.enphaseenergy.com/api/v3/systems/$siteID/pcus")
        .method("PUT", body)
        .addHeader("key", "33d4cec7b72267778b67d0bacded2753")
        .addHeader(
            "Authorization",
            "Bearer $token"
        )
        .addHeader("Content-Type", "application/json")
        .build()
    val response = client.newCall(request).execute()

}

fun addEncharge(encharge : String, siteID : String, envoy : String, token : String) {

    val client = OkHttpClient().newBuilder()
        .build()
    val mediaType = MediaType.parse("application/x-www-form-urlencoded")
    val body = RequestBody.create(
        mediaType,
        "system={\"encharge\":[{\"encharge_serial_numbers\":[\"$encharge\"],\"envoy_serial_number\":\"$envoy\",\"phase_values\":{}}],\"envoy_serial_numbers\":[\"$envoy\"]}"
    )
    val request: Request = Request.Builder()
        .url("https://api-qa2.enphaseenergy.com/api/v4/activations/$siteID")
        .method("PUT", body)
        .addHeader("Content-Type", "application/x-www-form-urlencoded")
        .addHeader(
            "Authorization",
            "Bearer $token"
        )
        .build()
    val response = client.newCall(request).execute()

}

fun addEnpower(enpower : String, siteID : String, envoy : String, token : String) {

    val client = OkHttpClient().newBuilder()
        .build()
    val mediaType = MediaType.parse("application/x-www-form-urlencoded")
    val body = RequestBody.create(
        mediaType,
        "system={\"backup_type\":2,\"partial_backup_type\":0,\"enpower\":[{\"enpower_serial_numbers\":[\"$enpower\"],\"envoy_serial_number\":\"$envoy\"}],\"envoy_serial_numbers\":[\"$envoy\"]}"
    )
    val request: Request = Request.Builder()
        .url("https://api-qa2.enphaseenergy.com/api/v4/activations/$siteID")
        .method("PUT", body)
        .addHeader("Content-Type", "application/x-www-form-urlencoded")
        .addHeader(
            "Authorization",
            "Bearer $token"
        )
        .build()
    val response = client.newCall(request).execute()

}

fun getSystemFromEnvoyNumber(envoyNumber: String, token: String): String? {
    val client = OkHttpClient().newBuilder()
        .build()
    val request = Request.Builder()
        .url("https://api-qa2.enphaseenergy.com/api/v4/systems/retrieve_system_id?serial_num=${envoyNumber}")
        .method("GET", null)
        .addHeader("key", "b4d162b4cd5334c7b63d1c75f8ed1650")
        .addHeader("Content-Type", "application/json")
        .addHeader(
            "Authorization",
            "Bearer $token"
        )
        .build()
    val response = client.newCall(request).execute()

    val yt = response.body()?.string()
    return if (yt == null) null else {
        val jObject = JSONObject(yt)
        if (jObject.has("system_id")) {
            return jObject["system_id"].toString()
        } else {
            return null
        }
    }
}

fun retireEnvoy(token: String, envoyNumber: String, systemID: String) {
    val client = OkHttpClient().newBuilder()
        .build()
    val mediaType = MediaType.parse("application/json")
    val body = RequestBody.create(mediaType, "[\"${envoyNumber}\"]")
    val request = Request.Builder()
        .url("https://api-qa2.enphaseenergy.com/api/v4/systems/${systemID}/envoys")
        .method("DELETE", body)
        .addHeader("key", "33d4cec7b72267778b67d0bacded2753")
        .addHeader(
            "Authorization",
            "Bearer $token"
        )
        .addHeader("Content-Type", "application/json")
        .build()
    val response = client.newCall(request).execute()
}