package com.enphase.itk_android_automation.utils

import android.content.ContentValues
import android.graphics.Bitmap
import android.os.Environment
import android.util.Log
import androidx.test.runner.screenshot.BasicScreenCaptureProcessor
import androidx.test.runner.screenshot.Screenshot
import org.junit.rules.TestWatcher
import org.junit.runner.Description
import java.io.File
import java.io.IOException

class ScreenshotTakingRule : TestWatcher() {

    override fun succeeded(description: Description) {
        // all good, tell everyone
    }

    override fun failed(e: Throwable, desc: Description) {
        try {
            Log.d(ContentValues.TAG, "Wasiq Screenshot")

            captureScreenshot(desc.methodName, desc.className)
        } catch (e1: IOException) {
            e1.printStackTrace()
        }
    }

    @Throws(IOException::class)
    private fun captureScreenshot(name: String, className: String) {

        val processor = BasicScreenCaptureProcessor()
        val capture = Screenshot.capture()
        capture.format = Bitmap.CompressFormat.PNG
        capture.name = name
        // capture.process()
        val filename = processor.process(capture)
        Log.d(ContentValues.TAG, "Wasiq Screenshot2")

        val folder =
            File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString() + "/screenshots/ITK/${className}")
        if (!folder.exists()) {
            folder.mkdirs()
        }

        Log.d(ContentValues.TAG, "Wasiq Screenshot3")

        val from =
            File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString() + "/screenshots/" + filename)
        val to =
            File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString() + "/screenshots/ITK/${className}/" + name + ".png")
        from.renameTo(to)
    }
}