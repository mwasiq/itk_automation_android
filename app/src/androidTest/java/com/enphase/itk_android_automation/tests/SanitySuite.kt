package com.enphase.itk_android_automation.tests

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.enphase.itk_android_automation.screens.BaseScreen
import com.enphase.itk_android_automation.screens.DevicesArrayScreen
import com.enphase.itk_android_automation.utils.*
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class SanitySuite : BaseTest() {

    val baseScreen = BaseScreen()

   @Test
    fun login(){

        baseScreen
            .installAppClean()
            .launchITKApp()
            .tapNextButtonInSplashScreen()
            .loginToITK()
            .tapAllow()
            .grantLocationPermission()
            .tapSkip()
            .grantStoragePermission()
            .tapGotIt()

    }

    @Test
    fun searchSystem(){

        baseScreen
            .installAppClean()
            .launchITKApp()
            .tapNextButtonInSplashScreen()
            .loginToITK()
            .tapAllow()
            .grantLocationPermission()
            .tapSkip()
            .grantStoragePermission()
            .tapGotIt()
            .goToSystem("WasiqHome")

    }

    @Test
    fun createSystem(){
        baseScreen
            .installAppClean()
            .launchITKApp()
            .tapNextButtonInSplashScreen()
            .loginToITK()
            .tapAllow()
            .grantLocationPermission()
            .tapSkip()
            .grantStoragePermission()
            .tapGotIt()
            .tapCreateNewSystem()
            .tapSystemDetails()
            .createSystemNA()

    }

    @Test
    fun editSystem(){
        baseScreen
            .installAppClean()
            .launchITKApp()
            .tapNextButtonInSplashScreen()
            .loginToITK()
            .tapAllow()
            .grantLocationPermission()
            .tapSkip()
            .grantStoragePermission()
            .tapGotIt()
            .goToSystem("WasiqHome4")
            .tapSystemDetails()
            .editSystem()
    }


    @Test
    fun deleteSystem(){

        val token = createAuthToken()
        val site = getRandomString(5)
        createSite(site, token!!)

        baseScreen
            .installAppClean()
            .launchITKApp()
            .tapNextButtonInSplashScreen()
            .loginToITK()
            .tapAllow()
            .grantLocationPermission()
            .tapSkip()
            .grantStoragePermission()
            .tapGotIt()
            .goToSystem(site)
            .deleteSyatem()

    }

    @Test
    fun addDevices(){

        val site = getRandomString(5)
        val token = createAuthToken()
        createSite(site, token!!)

        baseScreen
            .installAppClean()
            .launchITKApp()
            .tapNextButtonInSplashScreen()
            .loginToITK()
            .tapAllow()
            .grantLocationPermission()
            .tapSkip()
            .grantStoragePermission()
            .tapGotIt()
            .goToSystem(site)
            .tapDevicesDetails()
            .addDevices(1,1,1,1)
            .tapEnvoy()
            .scanDummyEnvoy()
            .tapMI()
            .scanDummyMicro()
            .tapEncharge()
            .scanDummyEncharge()
            .tapEnpower()
            .scanDummyEnpower()

    }



    @Test
    fun retireDevices(){

        val site = getRandomString(5)
        val envoy = "901${getRandomNumber(9)}"
        val pcu = "902${getRandomNumber(9)}"
        val encharge = "913${getRandomNumber(9)}"
        val enpower = "917${getRandomNumber(9)}"

        val token = createAuthToken()
        val siteID = createSite(site, token!!)

        addEnvoy(envoy, siteID!!, token)
        addPCU(pcu, siteID!!, token)
        addEncharge(encharge, siteID, envoy, token)
        addEnpower(enpower, siteID, envoy, token)

        baseScreen
            .installAppClean()
            .launchITKApp()
            .tapNextButtonInSplashScreen()
            .loginToITK()
            .tapAllow()
            .grantLocationPermission()
            .tapSkip()
            .grantStoragePermission()
            .tapGotIt()
            .goToSystem(site)
            .tapDevicesDetails2()
            .tapEnpower()
            .retireEnpower()
            .tapEncharge()
            .retireEncharge()
            .tapMI()
            .retireMI()
            .tapEnvoy()
            .retireEnvoy()

    }


    @Test
    fun envoySoftwareVersions(){

        baseScreen
            .installAppClean()
            .launchITKApp()
            .tapNextButtonInSplashScreen()
            .loginToITK()
            .tapAllow()
            .grantLocationPermission()
            .tapSkip()
            .grantStoragePermission()
            .tapGotIt()
            .navigateToMenu()
            .navigateToSettings()
            .validateEnvoySoftwareVersions()

    }


    @Test
    fun defaultGridProfile(){

        baseScreen
            .installAppClean()
            .launchITKApp()
            .tapNextButtonInSplashScreen()
            .loginToITK()
            .tapAllow()
            .grantLocationPermission()
            .tapSkip()
            .grantStoragePermission()
            .tapGotIt()
            .navigateToMenu()
            .navigateToSettings()
            .validateDefaultGridProfile()

    }

    @Test
    fun signOut(){
        baseScreen
            .installAppClean()
            .launchITKApp()
            .tapNextButtonInSplashScreen()
            .loginToITK()
            .tapAllow()
            .grantLocationPermission()
            .tapSkip()
            .grantStoragePermission()
            .tapGotIt()
            .navigateToMenu()
            .logOut()
            .verifyLoginScreen()

    }


}