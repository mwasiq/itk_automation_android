package com.enphase.itk_android_automation.tests

import android.Manifest
import androidx.test.rule.GrantPermissionRule
import androidx.test.uiautomator.Configurator
import com.enphase.itk_android_automation.utils.ScreenshotTakingRule
import org.junit.Before
import org.junit.Rule

open class BaseTest {

    @Rule
    @JvmField
    var mRuntimePermissionRule: GrantPermissionRule? = GrantPermissionRule
        .grant(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )


    @Before
    fun setUp() {
        //Configurator.getInstance().waitForSelectorTimeout = 0
    }

    @JvmField
    @Rule
    val screenshotRule = ScreenshotTakingRule()

}