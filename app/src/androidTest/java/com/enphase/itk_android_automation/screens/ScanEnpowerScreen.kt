package com.enphase.itk_android_automation.screens

import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiSelector
import androidx.test.uiautomator.Until
import com.enphase.itk_android_automation.utils.getRandomNumber

class ScanEnpowerScreen : BaseScreen() {

    private val editScan = "ivScanAction"
    private val serialNumberScan = "etSerialNumber"
    private val done = "bDone"
    private val fullBackUp = "rbFull"
    private val ok = "button1"
    private val no = "button2"
    private val retire = "btDeleteEnpower"
    private val okay = "btReport"

    fun scanDummyEnpower(){
        tapEditScan()
        val enp = "917${getRandomNumber(9)}"
        enterSerialNumber(enp)
        tapDone()
        tapDone()
        tapFullBackUp()
        tapSave()
        tapNo()
    }

    fun retireEnpower() : DevicesArrayScreen{
        val enp = waitForElementWithID(retire, 5000)
        enp.click()
        device.wait(Until.findObject(By.res("android:id/$ok")), 5000).click()
        val d = waitForElementWithID(okay, 6000)
        d.click()

        return DevicesArrayScreen()
    }

    private fun tapEditScan(){
        waitForElementWithID2(editScan, 3000).clickAndWait(Until.newWindow(), 2000)
    }


    private fun enterSerialNumber(serialNumber : String) {
        waitForElementWithID2(serialNumberScan, 3000).text = serialNumber
    }

    private fun tapFullBackUp(){
        waitForElementWithID(fullBackUp, 3000).click()
    }

    private fun tapDone(){
        waitForElementWithID(done, 3000).click()
        Thread.sleep(2000)
    }

    private fun tapSave() {
            device.wait(Until.findObject(By.res("android:id/$ok")), 5000).click()
    }

    private fun tapNo() {
        //device.wait(Until.findObject(By.res("android:id/$no")), 5000).click()
        val n = device.findObject(UiSelector().resourceId("android:id/$ok"))
        n.waitForExists(5000)
        n.click()
    }
}