package com.enphase.itk_android_automation.screens

import androidx.test.uiautomator.By
import androidx.test.uiautomator.Until
import com.enphase.itk_android_automation.utils.getRandomNumber

class ScanMicrosScreen : BaseScreen() {

    private val scanMI = "tvScanMI"
    private val editScan = "ivScanAction"
    private val serialNumberScan = "etSerialNumber"

    private val done1 = "bDone"
    private val doLater = "tvDoLater"
    private val table = "rvMicroInverters"
    private val pcu = "tvSerialNumber"
    private val retirePCU = "btDeleteMicroInv"
    private val ok = "button1"
    private val okay = "btReport"
    private val back = "Navigate up"

    fun scanDummyMicro() : DevicesArrayScreen{
        tapScanMI()
        grantCameraPermission()
        tapEditScan()
        val mi = "902${getRandomNumber(9)}"
        enterSerialNumber(mi)
        tapDone()
        tapDoitLater()
        return DevicesArrayScreen()
    }

    fun retireMI() : DevicesArrayScreen{
        val t = scrollableWithID(table)
        t.getChildByInstance(selectorByID(pcu), 0).click()
        val d = waitForElementWithID(retirePCU, 2000)
        d.click()
        device.wait(Until.findObject(By.res("android:id/$ok")), 5000).click()
        val dr = waitForElementWithID(okay, 6000)
        dr.click()

        Thread.sleep(4000)
        //waitForElementWithDesc(back, 2000).click()
        device.pressBack()
        return DevicesArrayScreen()
    }

    private fun tapScanMI(){
        waitForElementWithID2(scanMI, 2000).click()
    }

    private fun grantCameraPermission(){
        grantPermission("android.permission.CAMERA")
    }

    private fun tapEditScan(){
        waitForElementWithID2(editScan, 3000).click()
    }

    private fun enterSerialNumber(serialNumber : String) {
        //waitForElementWithID2(serialNumberScan, 2000).text = serialNumber
        //device.waitForIdle()
        Thread.sleep(2000)
        val t = elementWithID(serialNumberScan)
        t.waitForExists(2000)
        t.text = serialNumber
    }

    private fun tapDone(){
        waitForElementWithID2(done1, 2000).click()
    }

    private fun tapDoitLater(){
        waitForElementWithID2(doLater, 3000).click()
    }





}