package com.enphase.itk_android_automation.screens

import androidx.test.uiautomator.Until

class SplashScreen2 :BaseScreen() {

    private val progress = "progressbar"
    private val gotIt = "btCoachMarkGotIt"
   // private val gotIt = "Got it"

    fun tapGotIt() : HomeScreen{
       // waitForElementsWithID2(progress, 10000)
       // waitUntilElementWithIDGone2(progress, 10000)
        Thread.sleep(8000)
        waitForElementWithID(gotIt, 2000).click()
        return HomeScreen()
    }
}