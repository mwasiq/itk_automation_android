package com.enphase.itk_android_automation.screens

class LocationAccessScreen : BaseScreen() {

    private val skip = "btSkip"
    private val allow = "btAllow"

    fun tapSkip(){
        waitForElementWithID2(skip, 10000).click()
    }

    fun tapAllow() : LocationAccessScreen{
        waitForElementWithID2(allow, 10000).click()
        return LocationAccessScreen()
    }

    fun grantLocationPermission() : AutoDownloadScreen{
        Thread.sleep(2000)
        grantPermission("android.permission.ACCESS_FINE_LOCATION")
        grantPermission("android.permission.ACCESS_COARSE_LOCATION")

        return AutoDownloadScreen()
    }

}