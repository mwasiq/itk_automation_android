package com.enphase.itk_android_automation.screens

import android.util.Log
import androidx.test.uiautomator.By
import androidx.test.uiautomator.Until

class MenuScreen : BaseScreen(){

    private val logout = "layoutLogout"
    private val yes = "button1"
    private val settings = "Settings"

    fun logOut() : LoginScreen{
        waitForElementWithID(logout, 3000).click()
        device.wait(Until.findObject(By.res("android:id/$yes")), 5000).click()
        return LoginScreen()
    }

    fun navigateToSettings() : SettingsScreen{
        waitForElementWithText(settings, 5000).click()
        return SettingsScreen()
    }

}