package com.enphase.itk_android_automation.screens

import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiSelector
import androidx.test.uiautomator.Until

class DevicesArrayNumberScreen : BaseScreen(){


    private val counter = "counterView"
    private val increase = "ivAdd"
    private val envoy = "Envoy"
    private val micros = "Microinverters"
    private val encharge = "Encharge"
    private val enpower = "Enpower"
    private val done = "bDone"
    private val ok1 = "btReport"
    private val ok = "button1"
    private val progress = "pbProgressBar"


    fun addDevices(envoy : Int, micros : Int, encharge : Int, enpower : Int) : DevicesArrayScreen{
        increaseEnvoyCount(envoy)
        increaseMicroCount(micros)
        increaseEnchargeCount(encharge)
        increaseEnpowerCount(enpower)

        tapDone()
        tapOk1()
        tapOk()

        //waitForElementsWithID2(progress, 8000)
        //waitUntilElementWithIDGone2(progress, 8000)

        return DevicesArrayScreen()
    }

    private fun increaseEnvoyCount(count : Int){
        device.waitForIdle()
        for (i in 1..count) {
            waitForElementWithText(envoy, 2000).getFromParent(selectorByID(counter)).getChild(UiSelector().childSelector(selectorByID(increase))).click()
        }
    }


    private fun increaseMicroCount(count : Int){
        device.waitForIdle()
        for (i in 1..count) {
            elementWithText(micros).getFromParent(selectorByID(counter)).getChild(UiSelector().childSelector(selectorByID(increase))).click()
        }
    }

    private fun increaseEnchargeCount(count : Int){
        device.waitForIdle()
        for (i in 1..count) {
            elementWithTextContains(encharge).getFromParent(selectorByID(counter)).getChild(UiSelector().childSelector(selectorByID(increase))).click()
        }
    }

    private fun increaseEnpowerCount(count : Int){
        device.waitForIdle()
        for (i in 1..count) {
            elementWithText(enpower).getFromParent(selectorByID(counter)).getChild(UiSelector().childSelector(selectorByID(increase))).click()
        }
    }

    private fun tapDone(){
        device.waitForIdle()
        elementWithID(done).click()
    }

    private fun tapOk(){
        device.wait(Until.findObject(By.res("android:id/$ok")), 5000).click()
    }

    private fun tapOk1(){
        waitForElementWithID2(ok1, 10000).click()
    }
}

