package com.enphase.itk_android_automation.screens

class AutoDownloadScreen : BaseScreen() {

    private val skip = "btSkip"
    private val enable = "btEnable"

    fun tapEnable() : AutoDownloadScreen{
        waitForElementWithID2(enable, 2000).click()
        return AutoDownloadScreen()
    }

    fun tapSkip() : AutoDownloadScreen{
        //waitForElementWithID2(skip, 2000).click()
        Thread.sleep(2000)
        waitForElementWithText2("SKIP", 5000).click()
        return AutoDownloadScreen()
    }
    fun grantStoragePermission() : SplashScreen2{
        Thread.sleep(2000)
        grantPermission("android.permission.WRITE_EXTERNAL_STORAGE")
        grantPermission("android.permission.READ_EXTERNAL_STORAGE")
        return SplashScreen2()
    }


}