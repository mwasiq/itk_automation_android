package com.enphase.itk_android_automation.screens

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiCollection
import androidx.test.uiautomator.UiSelector
import androidx.test.uiautomator.Until

class CreateNewSystemScreen : BaseScreen() {


    private val row = "tvConfig"
    private val row1Name = "System Details"
    private val row2Name = "Devices & Array"
    private val table = "rvStatusList"
    private val container = "statusContainer"
    private val deleteSystem = "beDeleteSystem"
    private val delete = "button1"
    private val okay = "btReport"


    fun tapSystemDetails(): CreateSystemDetailsScreen {
        elementWithIDAndText(row, row1Name).waitForExists(10000)
        elementWithIDAndText(row, row1Name).clickAndWaitForNewWindow()
        return CreateSystemDetailsScreen()
    }

    fun tapDevicesDetails() : DevicesArrayNumberScreen{

        waitForElementWithID2(table, 20000)
        UiCollection(selectorByID(table)).getChildByInstance(selectorByID(container), 1).click()

        return DevicesArrayNumberScreen()
    }

    fun tapDevicesDetails2() : DevicesArrayScreen{

        waitForElementWithID2(table, 20000)
        UiCollection(selectorByID(table)).getChildByInstance(selectorByID(container), 1).click()

        return DevicesArrayScreen()
    }

    fun deleteSyatem(){
        waitForElementWithID2(table, 20000)
        val deleteButton = elementWithID(deleteSystem)
        scrollableWithID(table).scrollIntoView(deleteButton)
        deleteButton.click()
        device.wait(Until.findObject(By.res("android:id/$delete")), 3000).click()

        waitForElementWithID(okay, 10000).click()


    }
}