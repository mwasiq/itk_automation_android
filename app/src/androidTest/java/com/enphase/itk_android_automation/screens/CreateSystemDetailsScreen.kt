package com.enphase.itk_android_automation.screens

import androidx.test.uiautomator.Direction
import androidx.test.uiautomator.UiScrollable
import androidx.test.uiautomator.UiSelector
import androidx.test.uiautomator.Until
import com.enphase.itk_android_automation.utils.getRandomNumber
import com.enphase.itk_android_automation.utils.getRandomString

class CreateSystemDetailsScreen : BaseScreen() {

    private val table = "nsvEditSystems"
    private val systemName = "etSysName"
    private val refID = "etInstRefId"
    private val ownerType = "rgOwnerType"
    private val commercial = "rbCommercial"
    private val residential = "rbResidential"
    private val other = "rbOther"
    private val firstName = "etFirstName"
    private val lastName = "etLastName"
    private val email = "etEmail"
    private val mobile = "etMobile"
    private val country = "etCountry"
    private val countrySearch = "etSearch"
    private val countryRow = "cbItemName"
    private val countryDone = "tvDone"
    private val streetAdd1 = "etStreetAdd1"
    private val streetAdd2 = "etStreetAdd2"
    private val googleAddresses = "tvAddress"
    private val save = "btSave"
    private val state = "etState"
    private val stateSearch = "etSearch"
    private val stateRow = "cbItemName"
    private val stateDone = "tvDone"
    private val city = "etCity"
    private val zip = "etZipCode"
    private val edit = "tvEdit"

    private val save2 = "SAVE"
    private val ok = "btReport"
    private val ok2 = "OK"
    private val loading = "pbProgressBar"
    private val loading1 = "tvMessage"
    private val loading1Message = "Saving"
    private val loading2 = "tvMessage"
    private val loading2Message = "Loading"



    private val googleAddressesClassName = "DropDownListView"

    fun createSystemNA() : CreateNewSystemScreen{
        enterSiteName()
        enterRefID()
        selectOwnerType("Other")
        enterFirstName()
        enterLastName()
        enterEmail()
        enterMobile()
        selectCountry("United States")
        selectAddress("Astor Place")
        save()
        validateSystemCreatedSuccessfully()
        return CreateNewSystemScreen()
    }

    fun createSystemEMEA() : CreateNewSystemScreen{
        enterSiteName()
        enterRefID()
        selectOwnerType("Other")
        enterFirstName()
        enterLastName()
        enterEmail()
        enterMobile()
        selectCountry("Germany")
        selectAddress("Gartnerplatz")
        save()
        validateSystemCreatedSuccessfully()
        return CreateNewSystemScreen()

    }

    fun editSystem() : CreateNewSystemScreen{
        tapEdit()
        enterRefID()
        enterFirstName()
        enterLastName()
        enterEmail()
        enterMobile()
        save()
        validateSystemCreatedSuccessfully()
        return CreateNewSystemScreen()

    }


    private fun enterSiteName() {
        //waitForElementWithID2(table, 5000)
        val p = scrollableWithID(table)
        p.waitForExists(5000)
        val c = elementWithID(systemName)
        p.scrollIntoView(c)
        //c.text = faker.lorem().word()
        c.text = getRandomString(5)
    }

    private fun enterRefID() {
        //waitForElementWithID2(table, 2000)
        val p = scrollableWithID(table)
        p.waitForExists(5000)
        val c = elementWithID(refID)
        p.scrollIntoView(c)
        c.text = getRandomString(4)
    }

    private fun selectOwnerType(type : String) {
        //waitForElementWithID2(table, 2000)
        val p = scrollableWithID(table)
        p.waitForExists(5000)
        val c = elementWithID(ownerType)
        p.scrollIntoView(c)
        when(type) {
            "Other" -> elementWithID(other).click()
            "Commercial" -> elementWithID(commercial).click()
            "Residential" -> elementWithID(residential).click()
        }
    }

    private fun enterFirstName() {
        //waitForElementWithID2(table, 2000)
        val p = scrollableWithID(table)
        p.waitForExists(5000)
        val c = elementWithID(firstName)
        p.scrollIntoView(c)
        c.text = getRandomString(4)
    }

    private fun enterLastName() {
        //waitForElementWithID2(table, 2000)
        val p = scrollableWithID(table)
        p.waitForExists(5000)
        val c = elementWithID(lastName)
        p.scrollIntoView(c)
        c.text = getRandomString(4)
    }

    private fun enterEmail() {
        //waitForElementWithID2(table, 2000)
        val p = scrollableWithID(table)
        p.waitForExists(5000)
        val c = elementWithID(email)
        p.scrollIntoView(c)
        val r1 = getRandomString(4)
        val r2 = getRandomString(3)
        val r3 = getRandomString(3)

        c.text = "$r1.$r2@$r3.com"
    }


    private fun enterMobile() {
        //waitForElementWithID2(table, 2000)
        val p = scrollableWithID(table)
        p.waitForExists(5000)
        val c = elementWithID(mobile)
        p.scrollIntoView(c)
        c.text = "5414${getRandomNumber(6)}"
    }

    private fun selectCountry(countryName : String) {
        waitForElementWithID2(table, 2000)
        scrollableWithID(table).scrollIntoView(elementWithID(country))
        elementWithID(country).click()
        elementWithID(countrySearch).text = countryName
        elementWithIDAndText(countryRow, countryName).click()
        elementWithID2(countryDone).click()
    }

    private fun selectState(stateName : String) {
        waitForElementWithID2(table, 2000)
        scrollableWithID(table).scrollIntoView(elementWithID(country))
        elementWithID(state).click()
        elementWithID(stateSearch).text = stateName
        elementWithIDAndText(stateRow, stateName).click()
        elementWithID2(stateDone).click()
    }

    private fun selectAddress(address : String) {
        waitForElementWithID2(table, 2000)
        scrollableWithID(table).scrollIntoView(elementWithID(streetAdd1))
        elementWithID(streetAdd1).click()
        elementWithID(streetAdd1).text = address

        if(waitForElementWithID2Optional(googleAddresses, 8000) != null) {
            elementsWithID2(googleAddresses).first().click()
            device.pressBack()
            device.waitForIdle()
            waitForElementWithID2(streetAdd2, 5000).text = "wdw"
        }
        else{
            device.pressBack()
            selectState("California")
            elementWithID(city).text = "California"
            elementWithID(zip).text = "10003"

        }

    }


    private fun save(){
        waitForElementWithID2(table, 20000)
        scrollableWithID(table).scrollToEnd(10)
        elementWithID2(save).click()
    }

    private fun validateSystemCreatedSuccessfully(){
        waitForElementWithID2(ok, 10000).click()
    }


    private fun tapEdit(){
        waitForElementWithID2(edit, 5000).click()
    }


}