package com.enphase.itk_android_automation.screens

import androidx.test.uiautomator.By
import androidx.test.uiautomator.Until

class SettingsScreen : BaseScreen() {

    private val table = "rvEnvoyFirmware"
    private val envoyVersions = "tvVersion"
    private val defaultGridProfile = "Default Profile: CA Rule21 201902 VV VW FW (1.2.8)"
    private val defaultGridProfileID = "tvGridProfileName"


    fun validateEnvoySoftwareVersions() {
        waitForElementWithID(table, 5000)
        waitForElementWithText("04.10.35", 20000)
        waitForElementWithText("06.00.98", 20000)
        waitForElementWithText("06.01.55", 20000)
        waitForElementWithText("07.00.48", 20000)
        waitForElementWithText("07.01.03", 20000)

    }

    fun validateDefaultGridProfile() {
        waitForElementWithID(table, 5000)
        val t = scrollableWithID(table)
        t.scrollIntoView(selectorByID(defaultGridProfileID))
        waitForElementWithText(defaultGridProfile, 20000)

    }
}