package com.enphase.itk_android_automation.screens

class SplashScreen : BaseScreen() {

    private val gotItButton = "btnNext"


    fun tapNextButtonInSplashScreen(): LoginScreen {
        waitForElementWithID2(gotItButton, 40000).click()
        return LoginScreen()
    }
}