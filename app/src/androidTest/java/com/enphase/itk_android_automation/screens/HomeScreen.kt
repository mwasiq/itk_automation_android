package com.enphase.itk_android_automation.screens

import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiSelector
import androidx.test.uiautomator.Until

class HomeScreen : BaseScreen(){

    private val searchBar = "etSearch"
    private val table = "rvSystemsList"
    private val loading = "pbSearch"
    private val systemRow = "systemInfoLayout"
    private val createIcon = "fabAdd"

    private val menu = "menuMenu"


    fun goToSystem(system : String) : CreateNewSystemScreen {
        waitForElementWithID2(searchBar, 5000).text = system
        waitForElementWithID2(table, 5000)
        val g = scrollableWithID(table)
        val f = elementWithID(systemRow).getChild(UiSelector().text(system))
        f.waitForExists(6000)

        g.scrollIntoView(f)
        f.click()
        return CreateNewSystemScreen()

    }

    fun tapCreateNewSystem() : CreateNewSystemScreen {
        waitForElementWithID2(createIcon, 5000).wait(Until.clickable(true), 10000)
        elementWithID2(createIcon).click()
        return CreateNewSystemScreen()
    }

    fun navigateToMenu() : MenuScreen {
        waitForElementWithID(menu, 3000).click()
        return MenuScreen()
    }




}