package com.enphase.itk_android_automation.screens

import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiSelector
import androidx.test.uiautomator.Until
import com.enphase.itk_android_automation.utils.getRandomNumber

class ScanEnchargeScreen :BaseScreen() {

    private val scanEncharge = "btnScanEnc"
    private val editScan = "ivScanAction"
    private val serialNumberScan = "etSerialNumber"
    private val done1 = "bDone"
    private val doLater = "tvDoItLater"
    private val loading = "bProgressBar"
    private val phaseSelect = "etPhaseSelect"
    private val phaseTable = "rvOptions"
    private val phase = "cbItemName"
    private val phaseDone = "tvDone"
    private val table = "rvEncharges"
    private val encharge = "tvSerialNumber"
    private val retireEncharge = "btDeleteEncharge"
    private val ok = "button1"
    private val okay = "btReport"
    //private val back = "Navigate up"
    private val back = "ivBack"

    fun scanDummyEncharge() : DevicesArrayScreen{
        tapScanEncharge()
        tapEditScan()
        val enc = "913${getRandomNumber(9)}"
        enterSerialNumber(enc)
        tapDone()
        tapDoitLater()
        return DevicesArrayScreen()
    }

    fun retireEncharge() : DevicesArrayScreen{
        val t = scrollableWithID(table)
        t.getChildByInstance(selectorByID(encharge), 0).click()
        val d = waitForElementWithID(retireEncharge, 2000)
        d.click()
        device.wait(Until.findObject(By.res("android:id/$ok")), 5000).click()
        val dr = waitForElementWithID(okay, 6000)
        dr.click()

        Thread.sleep(4000)
        //waitForElementWithDesc(back, 2000).click()
        //waitForElementWithID(back, 4000).click()
        device.pressBack()
        return DevicesArrayScreen()
    }

    fun scanDummyEnchargeForEMEA() : DevicesArrayScreen{
        tapScanEncharge()
        tapEditScan()
        val enc = "913${getRandomNumber(9)}"
        enterSerialNumber(enc)
        tapDone()
        selectPhase()
        tapDoitLater()
        return DevicesArrayScreen()
    }

    private fun tapScanEncharge(){
        waitForElementWithID2(scanEncharge, 3000).click()
    }

    private fun tapEditScan(){
        waitForElementWithID2(editScan, 3000).clickAndWait(Until.newWindow(), 2000)
    }

    private fun enterSerialNumber(serialNumber : String) {
        waitForElementWithID2(serialNumberScan, 3000).text = serialNumber
    }

    private fun tapDone(){
        waitForElementWithID2(done1, 2000).click()
    }

    private fun tapDoitLater(){
        waitForElementWithID2(doLater, 3000).click()
        elementWithID(loading).waitForExists(2000)
        //waitForElementWithID2(loading, 5000)
        waitUntilElementWithIDGone2(loading, 8000)
       // device.waitForIdle()
       // Thread.sleep(10000)
    }

    private fun selectPhase(){
        waitForElementWithID2(phaseSelect, 2000).click()
        scrollableWithID(phaseTable).getChildByInstance(selectorByID(phase), 1).click()
        elementWithID(phaseDone).click()
    }





}