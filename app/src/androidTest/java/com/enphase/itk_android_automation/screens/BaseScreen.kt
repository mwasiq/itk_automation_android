package com.enphase.itk_android_automation.screens

import android.app.AppComponentFactory
import android.app.Instrumentation
import android.content.Context
import android.content.Intent
import android.net.wifi.WifiManager
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.*
//import com.github.javafaker.Faker

open class BaseScreen {

    private val instrumentation: Instrumentation = InstrumentationRegistry.getInstrumentation()
    val device: UiDevice = UiDevice.getInstance(instrumentation)
    private val packageName = "com.enphase.installer"
    private val appContext: Context = instrumentation.targetContext

    fun installAppClean() : BaseScreen{
        device.executeShellCommand("pm uninstall com.enphase.installer")
        device.executeShellCommand("pm install /data/local/tmp/ITK.apk")
        return BaseScreen()
    }

    /*fun grantStoragePermissions() : BaseScreen{
        device.executeShellCommand("pm grant com.enphase.installer android.permission.WRITE_EXTERNAL_STORAGE")
        device.executeShellCommand("pm grant com.enphase.installer android.permission.READ_EXTERNAL_STORAGE")
        return SplashScreen()
    }
    */

    fun grantPermission(permissionCommand : String) : BaseScreen{
        device.executeShellCommand("pm grant com.enphase.installer $permissionCommand")
        return BaseScreen()
    }

    fun revokePermission(permissionCommand : String) : BaseScreen{
        device.executeShellCommand("pm revoke com.enphase.installer $permissionCommand")
        return BaseScreen()
    }

    fun launchITKApp() : SplashScreen{
        val intent = appContext.packageManager.getLaunchIntentForPackage(packageName)?.apply { addFlags(
            Intent.FLAG_ACTIVITY_CLEAR_TASK) }
        appContext.startActivity(intent)
        device.wait(Until.hasObject(By.pkg(packageName).depth(0)), 40000)
        return SplashScreen()
    }

    fun activateITKApp() : BaseScreen{
        val intent = appContext.packageManager.getLaunchIntentForPackage(packageName)?.apply { addFlags(
            Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) }
        appContext.startActivity(intent)
        device.wait(Until.hasObject(By.pkg(packageName).depth(0)), 30000)
        return BaseScreen()
    }

    fun waitForElementWithID2(id: String, timeout: Long) : UiObject2 {
        return device.wait(Until.findObject(By.res(packageName, id)), timeout)
    }

    fun waitForElementWithID2Optional(id: String, timeout: Long) : UiObject2? {
        return device.wait(Until.findObject(By.res(packageName, id)), timeout)
    }


    fun waitForElementsWithID2(id: String, timeout: Long) : List<UiObject2> {
        return device.wait(Until.findObjects(By.res(packageName, id)), timeout)
    }

    fun waitForElementWithClass2(className: String, timeout: Long) : UiObject2 {
        return device.wait(Until.findObject(By.clazz(className)), timeout)
    }

    fun waitUntilElementWithIDGone2(id: String, timeout: Long) : Boolean {
        return device.wait(Until.gone(By.res(packageName, id)), timeout)
    }

    fun waitUntilElementWithIDGone(id: String, timeout: Long) {
        device.findObject(selectorByID(id)).waitUntilGone(timeout)
    }

    fun waitForElementWithID(id: String, timeout: Long) : UiObject {
        val u = device.findObject(UiSelector().resourceId("$packageName:id/$id"))
        u.waitForExists(timeout)
        return u
    }

    fun waitForElementWithDesc(desc: String, timeout: Long) : UiObject {
        val u = device.findObject(UiSelector().description(desc))
        u.waitForExists(timeout)
        return u
    }

    fun elementWithID2(id: String) : UiObject2 {
        return device.findObject(By.res(packageName, id))
    }

    fun elementsWithID2(id: String) : List<UiObject2> {
        return device.findObjects(By.res(packageName, id))
    }

    fun elementWithID(id: String) : UiObject {
        return device.findObject(UiSelector().resourceId("$packageName:id/$id"))
    }

    fun elementWithIDAndText(id: String, text : String) : UiObject {
        return device.findObject(UiSelector().resourceId("$packageName:id/$id").text(text))
    }

    fun elementWithIDAndTextContains(id: String, text : String) : UiObject {
        return device.findObject(UiSelector().resourceId("$packageName:id/$id").textContains(text))
    }

    fun scrollableWithID(id: String) : UiScrollable {
        return UiScrollable(UiSelector().resourceId("$packageName:id/$id"))
    }

    fun elementWithText2(text: String) : UiObject2 {
        return device.findObject(By.text(text))
    }

    fun elementWithText(text: String) : UiObject {
        return device.findObject(UiSelector().text(text))
    }

    fun elementWithTextContains(text: String) : UiObject {
        return device.findObject(UiSelector().textContains(text))
    }

    fun waitForElementWithText2(text: String, timeout: Long) : UiObject2 {
        return device.wait(Until.findObject(By.text(text)), timeout)
    }

    fun waitForElementWithText(text: String, timeout: Long) : UiObject {
        val e = device.findObject(UiSelector().text(text))
        e.waitForExists(timeout)
        return e
    }

    fun selectorByID(id: String) : UiSelector {
        return UiSelector().resourceId("$packageName:id/$id")
    }

    fun waitForElementWithTextContains2(text: String, timeout: Long) : UiObject2 {
        return device.wait(Until.findObject(By.textContains(text)), timeout)
    }

    fun waitForElementWithTextContains(text: String, timeout: Long) : UiObject {
        val e = device.findObject(UiSelector().textContains(text))
        e.waitForExists(timeout)
        return e
    }

    fun connectToWifi(ssid : String, password : String) : SplashScreen{
        val intent = Intent(WifiManager.ACTION_PICK_WIFI_NETWORK).apply { addFlags(Intent.FLAG_ACTIVITY_NEW_TASK) }
        appContext.startActivity(intent)
        val f = device.wait(Until.findObject(By.text(ssid)), 20000)
        val g = f.parent.children.last().text

        if(g.contains("Connected")){
           activateITKApp()
            //Do nothing


        }

        else if(g.contains("Saved") ){

            f.click()
            if (device.findObject(UiSelector().resourceId("com.android.settings:id/button1")).waitForExists(2000)){
                device.wait(Until.findObject(By.res("com.android.settings", "button1")), 2000).click()
                device.findObject(UiSelector().textContains(ssid)).waitUntilGone(2000)
            }
            else{

            }


            //val ff = device.wait(Until.findObject(By.text(ssid)), 20000)

            //ff.parent.children.last().wait(Until.textContains("Connected"), 10000)
            //device.wait(Until.hasObject(By.pkg(packageName)), 10000)
            activateITKApp()

        }

        else{

        }

      /*  when(g){
            "Connected" -> {
               // f.click()
               // device.wait(Until.hasObject(By.pkg(packageName)), 10000)

            }

            "Saved" -> {
                f.click()
                device.wait(Until.findObject(By.res("com.android.settings", "button1")), 2000).click()
                device.wait(Until.hasObject(By.pkg(packageName)), 10000)

            }

            else -> {
                f.clickAndWait(Until.newWindow(), 2000)
                device.wait(Until.findObject(By.res("com.android.settings", "password")), 2000).text = password
                device.wait(Until.findObject(By.res("com.android.settings", "button1")), 2000).click()
                device.wait(Until.hasObject(By.pkg(packageName)), 10000)


            }

        }

*/

        return SplashScreen()
    }

}