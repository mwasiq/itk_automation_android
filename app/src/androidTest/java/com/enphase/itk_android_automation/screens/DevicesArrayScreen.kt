package com.enphase.itk_android_automation.screens

import androidx.test.uiautomator.Until

class DevicesArrayScreen : BaseScreen(){

    private val table = "rvDeviceArray"
    private val envoy = "Envoy"
    private val row = "tvConfig"
    private val micros = "Microinverters"
    private val encharge = "Encharge"
    private val enpower = "Enpower"


    fun tapEnvoy() : ScanEnvoyScreen{
        waitForElementWithID(table, 20000)
        val f = elementWithIDAndText(row, envoy)
        scrollableWithID(table).scrollIntoView(f)
        f.click()
        return ScanEnvoyScreen()

    }

    fun tapMI() : ScanMicrosScreen{
        elementWithIDAndTextContains(row, micros).waitForExists(20000)
        val f = elementWithIDAndTextContains(row, micros)
        scrollableWithID(table).scrollIntoView(f)
        f.click()
        return ScanMicrosScreen()

    }

    fun tapEncharge() : ScanEnchargeScreen{
        elementWithIDAndTextContains(row, encharge).waitForExists(20000)
        val f = elementWithIDAndTextContains(row, encharge)
        scrollableWithID(table).scrollIntoView(f)
        f.click()
        return ScanEnchargeScreen()

    }

    fun tapEnpower() : ScanEnpowerScreen{

        waitForElementWithID2(table, 20000)
        scrollableWithID(table).scrollIntoView(elementWithIDAndText(row, enpower))
        elementWithIDAndText(row, enpower).click()
        return ScanEnpowerScreen()
    }
}