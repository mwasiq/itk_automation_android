package com.enphase.itk_android_automation.screens

import androidx.test.uiautomator.By
import androidx.test.uiautomator.Until
import com.enphase.itk_android_automation.utils.getRandomNumber

class ScanEnvoyScreen : BaseScreen() {


    private val edit = "ivScanAction"
    private val serial = "etSerialNumber"
    private val done = "bDone"
    private val gridDone = "btn_done"
    private val retireEnvoy = "btDeleteEnvoy"
    private val ok = "button1"
    private val okay = "btReport"


    fun scanDummyEnvoy() : DevicesArrayScreen{
        val sn = "901${getRandomNumber(9)}"
        tapEnterManuallyEditIcon()
        enterSerialNumber(sn)
        tapDone()
        tapDone()
        tapDoneForGridProfile()
        return DevicesArrayScreen()
    }

    fun retireEnvoy() {
        val enp = waitForElementWithID(retireEnvoy, 5000)
        enp.click()
        device.wait(Until.findObject(By.res("android:id/$ok")), 5000).click()
        val d = waitForElementWithID(okay, 6000)
        d.click()
    }

    private fun tapEnterManuallyEditIcon(){
        waitForElementWithID(edit, 3000).click()
    }

    private fun enterSerialNumber(serialNumber : String){
        waitForElementWithID(serial, 5000).text = serialNumber
    }

    private fun tapDone(){
        waitForElementWithID(done, 3000).click()
        Thread.sleep(2000)
    }

    private fun tapDoneForGridProfile(){
        waitForElementWithID2(gridDone, 10000).wait(Until.enabled(true), 10000)
        elementWithID(gridDone).click()
    }

}