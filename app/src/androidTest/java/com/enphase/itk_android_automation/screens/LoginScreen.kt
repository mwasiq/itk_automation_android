package com.enphase.itk_android_automation.screens

class LoginScreen : BaseScreen(){

    private val email = "etEmail"
    private val password = "etPassword"
    private val login = "btLogin"
    private val QA2 = "QA2"

    fun loginToITK() : LocationAccessScreen{
        waitForElementWithID2(password, 1000).text = "<connection>"
        elementWithID2(login).click()
        waitForElementWithText2(QA2, 1000).click()
        waitForElementWithID2(email, 2000).text = "mwasiq@enphaseenergy.com"
        elementWithID2(password).text = "Enphase@3159"
        elementWithID2(login).click()

        return LocationAccessScreen()
    }

    fun verifyLoginScreen() {
        waitForElementWithID2(email, 10000)

    }
}